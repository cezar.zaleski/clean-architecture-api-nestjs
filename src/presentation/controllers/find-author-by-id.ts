import { Controller, HttpResponse, notFound, ok, serverError } from '@/presentation/contracts'
import { AuthorViewModel } from '@/presentation/view-models'
import { NotFound } from '@/domain/errors/not-found';
import { FindAuthorById } from '@/domain/usecases/author/find-author-by-id';

export class FindAuthorByIdController implements Controller {
  constructor(private readonly findByIdAuthor: FindAuthorById) {}

  async handle(request: FindAuthorByIdController.Request): Promise<HttpResponse<[AuthorViewModel]>> {
    try {
      const authors = await this.findByIdAuthor.run(request.id)
      return ok(authors)
    } catch (error) {
      if (error instanceof NotFound ) return notFound(error)
      return serverError(error)
    }
  }
}

export namespace FindAuthorByIdController {
  export type Request = {
    id: number
  }
}
