import { badRequest, Controller, HttpResponse, ok, serverError } from '@/presentation/contracts'
import { AuthorViewModel } from '@/presentation/view-models'
import Author from '@/domain/entities/author';
import { ApiProperty } from '@nestjs/swagger';
import { EmptyParamError } from '@/domain/errors';
import { SaveAuthor } from '@/domain/usecases/author';

export class SaveAuthorController implements Controller {
  constructor(private readonly saveAuthor: SaveAuthor) {}

  async handle(request: SaveAuthorController.Request): Promise<HttpResponse<AuthorViewModel>> {
    try {
      const author = await this.saveAuthor.run(SaveAuthorController.toAuthor(request.author))
      return ok(author)
    } catch (error) {
      if (error instanceof EmptyParamError ) return badRequest(error)
      return serverError(error)
    }
  }
}

export namespace SaveAuthorController {
  export type Request = {
    author: CreateAuthor
  }
  export const toAuthor = (createAuthor :CreateAuthor): Author => {
    return new Author(createAuthor.name, null, null, null);
  }

  export class CreateAuthor {

    @ApiProperty()
    name: string;
  }

}
