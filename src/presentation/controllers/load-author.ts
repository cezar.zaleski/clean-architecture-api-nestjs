import { Controller, HttpResponse, ok, serverError } from '@/presentation/contracts'
import { AuthorViewModel } from '@/presentation/view-models'
import { ApiProperty } from '@nestjs/swagger';
import { LoadAuthor } from '@/domain/usecases/author';

export class LoadAuthorController implements Controller {
  constructor(private readonly loadAuthor: LoadAuthor) {}

  async handle(): Promise<HttpResponse<[AuthorViewModel]>> {
    try {
      const authors = await this.loadAuthor.run()
      return ok(authors)
    } catch (error) {
      return serverError(error)
    }
  }
}

export namespace LoadAuthorController {
  export type Request = {
    author: CreateAuthor
  }

  export class CreateAuthor {

    @ApiProperty()
    name: string;
  }

}
