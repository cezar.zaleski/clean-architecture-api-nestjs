export type AuthorViewModel = {
  id: number,
  name: string,
  active: boolean,
  date: Date,
}
