import { Database } from '@/infra/contracts'
import * as path from 'path'
import { createConnection } from 'typeorm'
import env from '@/main/config/env';

export class MysqlDatabase implements Database {
  async connect() {
    const database = await createConnection({
      type: 'mysql',
      host: env.databases.mysql.host,
      database: env.databases.mysql.database,
      username: env.databases.mysql.user,
      password: env.databases.mysql.password,
      port: +env.databases.mysql.port,
      synchronize: true,
      entities: [path.resolve(__dirname, './entities/*-entity{.ts,.js}')],
    })

    return database
  }
}
