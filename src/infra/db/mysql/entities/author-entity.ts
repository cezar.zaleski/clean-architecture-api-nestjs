import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
import Author from '@/domain/entities/author';


@Entity({name: 'autor'})
export class MysqlAuthorEntity {
  @PrimaryGeneratedColumn({name: 'id_autor'})
  idAutor: number;

  @Column({name: 'no_autor'})
  noAutor: string;

  @Column({name: 'st_ativo'})
  stAtivo: number;

  @Column({name: 'dt_cadastro'})
  dtCadastro?: Date;

  static fromAuthor(author: Author): MysqlAuthorEntity {
    return {
      idAutor: author.id,
      noAutor: author.name,
      stAtivo: author.active ? 1 : 0,
      dtCadastro: author.date
    }
  }

  static toAuthor(authorEntity: MysqlAuthorEntity): Author {
    return {
      id: authorEntity.idAutor,
      name: authorEntity.noAutor,
      active: authorEntity.stAtivo == 1,
      date: authorEntity.dtCadastro
    }
  }
}
