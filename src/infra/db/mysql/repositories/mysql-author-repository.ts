import { MysqlAuthorEntity } from '@/infra/db/entities'
import { getManager } from 'typeorm'
import { Repository } from 'typeorm/repository/Repository';
import { LoadAuthorRepository, SaveAuthorRepository } from '@/domain/usecases/author';
import Author from '@/domain/entities/author';
import { FindAuthorByIdRepository } from '@/domain/usecases/author/find-author-by-id-repository';

export class MysqlAuthorRepository implements SaveAuthorRepository, LoadAuthorRepository, FindAuthorByIdRepository {
  repository: Repository<MysqlAuthorEntity>
  constructor() {
    this.repository = getManager().getRepository(MysqlAuthorEntity)
  }

  async save(author: Author): Promise<Author> {
    const authorSaved = await this.repository.save(MysqlAuthorEntity.fromAuthor(author))
    author.id = authorSaved.idAutor
    return author;
  }

  async load(): Promise<Author[]> {
    const authors = await this.repository.find({})
    if (!authors.length) return []
    return authors.map(a => MysqlAuthorEntity.toAuthor(a))
  }

  async find(id: number): Promise<Author> {
    const author = await this.repository.findOne({idAutor: id})
    if (!author) return
    return MysqlAuthorEntity.toAuthor(author)
  }
}
