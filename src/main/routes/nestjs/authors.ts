
import { adaptNestJSResolver } from '@/main/adapters/nestjs-router'
import { Body, Controller, Get, Param, Post } from '@nestjs/common'
import { MysqlAuthorRepository } from '@/infra/db/mysql/repositories';
import { saveAuthorController } from '@/main/factories/save-author-controller';
import { SaveAuthorController } from '@/presentation/controllers';
import { loadAuthorController } from '@/main/factories/load-author-controller';
import { findAuthorByIdController } from '@/main/factories/find-author-by-id-controller';

@Controller('authors')
export class AuthorsController {

  @Post('')
  saveAuthor(@Body() author: SaveAuthorController.Request) {
    const repository = new MysqlAuthorRepository()
    return adaptNestJSResolver(saveAuthorController(repository), { author })
  }

  @Get('')
  loadAuthors() {
    const repository = new MysqlAuthorRepository()
    return adaptNestJSResolver(loadAuthorController(repository), {})
  }

  @Get(':id')
  findAuthorById(@Param('id') id: number) {
    const repository = new MysqlAuthorRepository()
    return adaptNestJSResolver(findAuthorByIdController(repository), {id})
  }
}
