import { MysqlDatabase } from '@/infra/db'
import env from '@/main/config/env'

const chooseDatabase = (dbType: string) => {
  if (dbType === 'mysql') return new MysqlDatabase()
  return new MysqlDatabase()
}

const chooseFramework = (frameworkType: string) => {
  if (frameworkType === 'nestjs')
    return import('@/main/config/frameworks/nestjs').then((framework) => framework.nestApp())
  return import('@/main/config/frameworks/nestjs').then((framework) => framework.nestApp())
}

const startServer = async () => {
  const database = chooseDatabase(env.currentDatabase)
  const app = await chooseFramework(env.currentFramework)

  database
    .connect()
    .then(async () => {
      await app.listen(env.port, () => console.log(`server running at: http://localhost:${env.port}/api`))
    })
    .catch((error) => {
      console.log(`database connection problem: ${error}`)
    })
}

startServer()
