import { Controller } from '@/presentation/contracts'
import { HttpException, HttpStatus } from '@nestjs/common';

export const adaptNestJSResolver = async (controller: Controller, params: any): Promise<any> => {
  const httpResponse = await controller.handle(params)
  if ([HttpStatus.OK, HttpStatus.NO_CONTENT].includes(httpResponse.statusCode)) return httpResponse
  throw new HttpException(httpResponse.body.message, httpResponse.statusCode)
}
