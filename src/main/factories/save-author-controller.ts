import { Controller } from '@/presentation/contracts'
import { SaveAuthorController } from '@/presentation/controllers';
import { SaveAuthor, SaveAuthorRepository } from '@/domain/usecases/author';

export const saveAuthorController = (repository: SaveAuthorRepository): Controller => {
  const saveAuthor = new SaveAuthor(repository)
  return new SaveAuthorController(saveAuthor)
}
