import { Controller } from '@/presentation/contracts'
import { LoadAuthorController } from '@/presentation/controllers';
import { LoadAuthor, LoadAuthorRepository } from '@/domain/usecases/author';

export const loadAuthorController = (repository: LoadAuthorRepository): Controller => {
  const loadAuthorService = new LoadAuthor(repository)
  return new LoadAuthorController(loadAuthorService)
}
