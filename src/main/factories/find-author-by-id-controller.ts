import { Controller } from '@/presentation/contracts'
import { FindAuthorByIdRepository } from '@/domain/usecases/author/find-author-by-id-repository';
import { FindAuthorByIdController } from '@/presentation/controllers/find-author-by-id';
import { FindAuthorById } from '@/domain/usecases/author/find-author-by-id';

export const findAuthorByIdController = (repository: FindAuthorByIdRepository): Controller => {
  const findAuthorById = new FindAuthorById(repository)
  return new FindAuthorByIdController(findAuthorById)
}
