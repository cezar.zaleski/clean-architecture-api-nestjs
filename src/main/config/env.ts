import * as dotenv from 'dotenv'

dotenv.config()

export default {
  port: process.env.PORT || 3000,
  currentDatabase: process.env.CURRENT_DATABASE,
  currentFramework: process.env.CURRENT_FRAMEWORK,
  databases: {
    mysql: {
      host: process.env.MYSQL_HOST,
      user: process.env.MYSQL_USER,
      password: process.env.MYSQL_PASSWORD,
      port: process.env.MYSQL_PORT,
      database: process.env.MYSQL_DATABASE,
    },
    mongodb: {
      url: process.env.MONGODB_URL,
      database: process.env.MONGODB_DATABASE,
    },
  },
}
