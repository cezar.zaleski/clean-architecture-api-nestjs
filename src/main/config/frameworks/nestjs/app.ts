import { Module } from '@nestjs/common'
import { NestFactory } from '@nestjs/core'
import { AuthorsModule } from '@/main/config/frameworks/nestjs/author-module';

@Module({
  imports: [AuthorsModule],
})
class AppModule {}

export const NestApp = NestFactory.create(AppModule)
