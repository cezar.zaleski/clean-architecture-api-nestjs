import { AuthorsController } from '@/main/routes/nestjs'
import { Module } from '@nestjs/common'

@Module({
  controllers: [AuthorsController],
})
export class AuthorsModule {}
