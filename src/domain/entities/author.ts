import { EmptyParamError } from '@/domain/errors';

export default class Author {
  id?: number;
  name: string;
  active?: boolean;
  date?: Date;

  constructor (name: string, id?: number, active?: boolean, date?: Date) {
    if (!name) throw new EmptyParamError('name')
    this.id = id;
    this.name = name;
    this.active = active !== undefined ?? true;
    this.date = date ?? new Date();
  }
}
