export class NotFound extends Error {
  constructor(param: string) {
    super(`NotFound ${param}`)
  }
}
