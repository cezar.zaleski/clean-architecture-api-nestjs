import Author from '@/domain/entities/author';
import { SaveAuthorRepository } from '@/domain/usecases/author/save-author-repository';


export class SaveAuthor {
  constructor(private readonly saveAuthorRepository: SaveAuthorRepository) {}

  run(author: Author): Promise<Author> {
    return this.saveAuthorRepository.save(author)
  }
}
