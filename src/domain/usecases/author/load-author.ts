import Author from '@/domain/entities/author';
import { LoadAuthorRepository } from '@/domain/usecases/author/load-author-repository';


export class LoadAuthor {
  constructor(private readonly loadAuthorRepository: LoadAuthorRepository) {}

  run(): Promise<Author[]> {
    return this.loadAuthorRepository.load()
  }
}
