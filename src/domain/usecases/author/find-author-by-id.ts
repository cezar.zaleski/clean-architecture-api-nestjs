import Author from '@/domain/entities/author';
import { NotFound } from '@/domain/errors/not-found';
import { FindAuthorByIdRepository } from '@/domain/usecases/author/find-author-by-id-repository';


export class FindAuthorById {
  constructor(private readonly findByIdAuthorRepository: FindAuthorByIdRepository) {}

  async run(id: number): Promise<Author> {
    const author = await this.findByIdAuthorRepository.find(id)
    if (author) return author
    throw new NotFound('author')
  }
}
