import Author from '@/domain/entities/author';

export interface SaveAuthorRepository {
  save: (author: Author) => Promise<Author>
}
