import Author from '@/domain/entities/author';


export interface LoadAuthorRepository {
  load: () => Promise<Author[]>
}
