import Author from '@/domain/entities/author';


export interface FindAuthorByIdRepository {
  find: (id: number) => Promise<Author>
}
