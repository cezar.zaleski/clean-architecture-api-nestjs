module.exports = {
  collectCoverageFrom: [
    '<rootDir>/src/**/*.ts',
    '!<rootDir>/src/main/**',
    '<rootDir>/src/main/routes/*',
    '!<rootDir>/src/**/index.ts',
    '!<rootDir>/src/**/connection.ts',
    '!<rootDir>/src/**/**-entity.ts',
    '!<rootDir>/src/**/entities.ts',
    '!<rootDir>/src/infra/db/repositories.ts',
  ],
  coverageDirectory: 'coverage',
  coverageProvider: 'babel',
  moduleNameMapper: {
    '@/tests/(.+)': '<rootDir>/tests/$1',
    '@/(.+)': '<rootDir>/src/$1'
  },
  testMatch: ['**/*.spec.ts', '**/*.test.ts'],
  roots: [
    '<rootDir>/src',
    '<rootDir>/tests'
  ],
  transform: {
    '\\.ts$': 'ts-jest'
  },
  clearMocks: true,
}

