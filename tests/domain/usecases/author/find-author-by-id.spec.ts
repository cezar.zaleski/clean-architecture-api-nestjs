import { mock, MockProxy } from 'jest-mock-extended';
import Author from '@/domain/entities/author';
import { NotFound } from '@/domain/errors/not-found';
import { FindAuthorById } from '@/domain/usecases/author/find-author-by-id';
import { FindAuthorByIdRepository } from '@/domain/usecases/author/find-author-by-id-repository';


describe('FindByIdAuthor',  () => {
  let sut: FindAuthorById;
  let findAuthorRepository: MockProxy<FindAuthorByIdRepository>
  const author: Author = {
    id: 10,
    name: 'author',
    date: new Date(),
    active: true
  }

  beforeAll(() => {
    findAuthorRepository = mock()
    findAuthorRepository.find.mockResolvedValue(author)
  })

  beforeEach(() => {
    sut = new FindAuthorById(findAuthorRepository)
  })

  it('should find by id author on success', async () => {
    const result = await sut.run(10)
    expect(result).toEqual(author)
  })

  it('should find by id author on nonexists', async () => {
    findAuthorRepository.find.mockResolvedValue(undefined)
    const result = sut.run(10)
    await expect(result).rejects.toThrow(new NotFound('author'))
  })
})
