import { mock, MockProxy } from 'jest-mock-extended';
import { SaveAuthor, SaveAuthorRepository } from '@/domain/usecases/author';
import Author from '@/domain/entities/author';


describe('SaveAuthorService',  () => {
  let sut: SaveAuthor;
  let saveAuthorRepository: MockProxy<SaveAuthorRepository>
  const author: Author = {
    id: 10,
    name: 'author',
    date: new Date(),
    active: true
  }

  beforeAll(() => {
    saveAuthorRepository = mock()
    saveAuthorRepository.save.mockResolvedValue(author)
  })

  beforeEach(() => {
    sut = new SaveAuthor(saveAuthorRepository)
  })

  it('should created author on success', async () => {
    const result = await sut.run(author)
    expect(result).toBe(author)
  })
})
