import { mock, MockProxy } from 'jest-mock-extended';
import { LoadAuthor, LoadAuthorRepository } from '@/domain/usecases/author';
import Author from '@/domain/entities/author';


describe('LoadAuthorService',  () => {
  let sut: LoadAuthor;
  let loadAuthorRepository: MockProxy<LoadAuthorRepository>
  const author: Author = {
    id: 10,
    name: 'author',
    date: new Date(),
    active: true
  }

  beforeAll(() => {
    loadAuthorRepository = mock()
    loadAuthorRepository.load.mockResolvedValue([author])
  })

  beforeEach(() => {
    sut = new LoadAuthor(loadAuthorRepository)
  })

  it('should created author on success', async () => {
    const result = await sut.run()
    expect(result).toEqual([author])
  })
})
