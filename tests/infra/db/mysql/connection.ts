import { createConnection } from 'typeorm';
import path from 'path';


export const makeTestDb = async (entities?: any[]): Promise<any> => {
  const connection = await createConnection({
    type: 'mysql',
    host: 'localhost',
    username: 'test',
    password: 'test',
    // logging: 'all',
    port: 3306,
    database: 'testdb',
    entities: entities ?? [path.resolve(__dirname.replace('/tests', ''), '../../../src/infra/db/mysql/entities/*-entity{.ts,.js}')],
    synchronize: true,
  })
  await connection.synchronize()
}
