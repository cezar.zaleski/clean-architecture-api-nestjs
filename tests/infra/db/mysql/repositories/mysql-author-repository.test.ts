import { getConnection, getManager } from 'typeorm';
import { makeTestDb } from '@/tests/infra/db/mysql/connection';
import { MysqlAuthorEntity } from '@/infra/db/mysql/entities';
import { MysqlAuthorRepository } from '@/infra/db/mysql/repositories';
import { Repository } from 'typeorm/repository/Repository';
import Author from '@/domain/entities/author';


beforeAll(async () => {
  await makeTestDb([MysqlAuthorEntity])
})

afterAll(async () => {
  await getConnection().close()
})

describe('MysqlAuthorRepository', () => {
  let sut: MysqlAuthorRepository;
  let repository: Repository<MysqlAuthorEntity>
  beforeAll(async () => {
    sut = new MysqlAuthorRepository()
    repository = getManager().getRepository(MysqlAuthorEntity)
    await repository.delete({})
  })
  it('Should SaveAuthorRepository on success', async () => {
    const author: Author = {
      id: 10,
      name: 'author',
      date: new Date(),
      active: true
    }
    const result = await sut.save(author)
    expect(result).toBe(author)
  })
  it('Should LoadAuthorRepository on success', async () => {
    const author: Author = {
      name: 'author',
      date: new Date(),
      active: true
    }
    await repository.delete({})
    await repository.save(MysqlAuthorEntity.fromAuthor(author))
    const result = await sut.load()
    expect(result[0].id).not.toBeNull()
    delete result[0].id
    author.date = result[0].date
    expect(result).toEqual([author])
  })
  it('Should FindByIdAuthorRepository on success', async () => {
    const author: Author = {
      name: 'author',
      date: new Date(),
      active: true
    }
    await repository.delete({})
    const authorMysql = await repository.save(MysqlAuthorEntity.fromAuthor(author))
    const result = await sut.find(authorMysql.idAutor)
    expect(result.id).not.toBeNull()
    delete result.id
    author.date = result.date
    expect(result).toEqual(author)
  })

  it('Should FindByIdAuthorRepository not found', async () => {
    const result = await sut.find(-99)
    expect(result).toBeUndefined()
  })

})
