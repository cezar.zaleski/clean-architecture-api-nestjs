import { LoadAuthorController } from '@/presentation/controllers';
import { mock, MockProxy } from 'jest-mock-extended';
import { ok, serverError } from '@/presentation/contracts';
import { LoadAuthor, LoadAuthorRepository } from '@/domain/usecases/author';
import Author from '@/domain/entities/author';

describe('LoadAuthor Controller', () => {
  let sut: LoadAuthorController
  let loadAuthorRepository: MockProxy<LoadAuthorRepository>
  let loadAuthor: MockProxy<LoadAuthor>

  const author: Author = {
    name: 'author-test',
    date: new Date(),
    active: true
  }
  beforeEach(() => {
    loadAuthor = mock()
    loadAuthorRepository = mock()
    loadAuthorRepository.load.mockResolvedValue([author])
    loadAuthor.run.mockResolvedValue([author])
    sut = new LoadAuthorController(loadAuthor)
  })

  it('Should return 200 on success', async () => {
    const httpResponse = await sut.handle()
    expect(httpResponse).toEqual(ok([author]))
  })

  it('Should return 500 server error', async () => {
    loadAuthor.run.mockImplementation(() => {
      throw new Error('Internal server error');
    });
    const httpResponse = await sut.handle()
    expect(httpResponse).toEqual(serverError(new Error('Internal server error')))
  })
})
