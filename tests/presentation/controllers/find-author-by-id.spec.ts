import { mock, MockProxy } from 'jest-mock-extended';
import { notFound, ok, serverError } from '@/presentation/contracts';
import Author from '@/domain/entities/author';
import { FindAuthorByIdController } from '@/presentation/controllers/find-author-by-id';
import { FindAuthorByIdRepository } from '@/domain/usecases/author/find-author-by-id-repository';
import { FindAuthorById } from '@/domain/usecases/author/find-author-by-id';
import { NotFound } from '@/domain/errors/not-found';

describe('FindAuthorById Controller', () => {
  let sut: FindAuthorByIdController
  let findAuthorByIdRepository: MockProxy<FindAuthorByIdRepository>
  let findAuthorById: MockProxy<FindAuthorById>

  const author: Author = {
    name: 'author-test',
    date: new Date(),
    active: true
  }
  beforeEach(() => {
    findAuthorById = mock()
    findAuthorByIdRepository = mock()
    findAuthorById.run.mockResolvedValue(author)
    sut = new FindAuthorByIdController(findAuthorById)
  })

  it('Should return 200 on success', async () => {
    findAuthorByIdRepository.find.mockResolvedValue(author)
    const httpResponse = await sut.handle({id: 99})
    expect(httpResponse).toEqual(ok(author))
  })

  it('Should return 500 server error', async () => {
    findAuthorById.run.mockImplementation(() => {
      throw new Error('Internal server error');
    });
    const httpResponse = await sut.handle({id: 10})
    expect(httpResponse).toEqual(serverError(new Error('Internal server error')))
  })

  it('Should return 404 author nonexists', async () => {
    findAuthorById.run.mockImplementation(() => {
      throw new NotFound('author');
    });
    const httpResponse = await sut.handle({id: 10})
    expect(httpResponse).toEqual(notFound(new NotFound('author')))
  })
})
