import { SaveAuthorController } from '@/presentation/controllers';
import { mock, MockProxy } from 'jest-mock-extended';
import { badRequest, ok, serverError } from '@/presentation/contracts';
import { EmptyParamError } from '@/domain/errors';
import { SaveAuthor, SaveAuthorRepository } from '@/domain/usecases/author';
import Author from '@/domain/entities/author';

describe('SaveAuthor Controller', () => {
  let sut: SaveAuthorController
  let saveAuthorRepository: MockProxy<SaveAuthorRepository>
  let saveAuthor: MockProxy<SaveAuthor>

  const author: Author = {
    name: 'author-test',
    date: new Date(),
    active: true
  }
  let request: SaveAuthorController.Request;
  beforeEach(() => {
    request = {author: author}
    saveAuthor = mock()
    saveAuthorRepository = mock()
    saveAuthorRepository.save.mockResolvedValue(author)
    saveAuthor.run.mockResolvedValue(author)
    sut = new SaveAuthorController(saveAuthor)
  })

  it('Should return 200 on success', async () => {
    const httpResponse = await sut.handle(request)
    expect(httpResponse).toEqual(ok(author))
  })

  it('Should return 400 if Validation fails', async () => {
    const requestFail = {author: {...author}}
    delete requestFail.author.name
    const httpResponse = await sut.handle(requestFail)
    expect(httpResponse).toEqual(badRequest(new EmptyParamError('name')))
  })
  it('Should return 500 server error', async () => {
    saveAuthor.run.mockImplementation(() => {
      throw new Error('Internal server error');
    });
    const httpResponse = await sut.handle(request)
    expect(httpResponse).toEqual(serverError(new Error('Internal server error')))
  })
})
