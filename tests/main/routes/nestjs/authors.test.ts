import request from 'supertest'
import { nestApp } from '@/main/config/frameworks';
import { getConnection, getManager } from 'typeorm';
import { makeTestDb } from '@/tests/infra/db/mysql/connection';
import { MysqlAuthorEntity } from '@/infra/db/mysql/entities';
import { INestApplication } from '@nestjs/common';
import { Repository } from 'typeorm/repository/Repository';
import Author from '@/domain/entities/author';

describe('Authors Routes', () => {
  let app: INestApplication;
  let repository: Repository<MysqlAuthorEntity>
  beforeAll(async () => {
    jest.setTimeout(1000)
    app = await nestApp()
    await app.init()
    await makeTestDb([MysqlAuthorEntity])
    repository = getManager().getRepository(MysqlAuthorEntity)
  })

  afterAll(async () => {
    await repository.delete({})
    await getConnection().close()
  })
  describe('POST /authors', () => {
    it('should return 201 create author', async () => {
      const { status, body } = await request(app.getHttpServer())
        .post('/api/authors')
        .send({ name: 'author_test' })
      expect(status).toBe(201)
      expect(body.id).not.toBeNull()
    })
    it('should return 400 validation name author fail', async () => {
      const { status } = await request(app.getHttpServer())
        .post('/api/authors')
        .send()
      expect(status).toBe(400)
    })
  })
  describe('GET /authors', () => {
    let author: Author;
    let authorMysql;
    beforeEach(async () => {
      author = {
        name: 'test',
        active: true,
        date: new Date()
      }
      await repository.delete({})
      authorMysql = await repository.save(MysqlAuthorEntity.fromAuthor(author))
    })
    it('should return 200 load authors', async () => {
      const { status, body } = await request(app.getHttpServer())
        .get('/api/authors')
        .send()
      expect(status).toBe(200)
      author.id = authorMysql.idAutor
      author.date = body.body[0].date
      expect(body.body).toEqual([author])
    })
  })
  describe('GET /authors/id', () => {
    let author: Author;
    let authorMysql: MysqlAuthorEntity;
    beforeEach(async () => {
      author = {
        name: 'test',
        active: true,
        date: new Date()
      }
      await repository.delete({})
      authorMysql = await repository.save(MysqlAuthorEntity.fromAuthor(author))
    })
    it('should return 200 find author by id', async () => {
      const { status, body } = await request(app.getHttpServer())
        .get(`/api/authors/${authorMysql.idAutor}`)
        .send()
      expect(status).toBe(200)
      author.id = authorMysql.idAutor
      author.date = body.body.date
      expect(body.body).toEqual(author)
    })
  })
})
